from lib.db_manager import DatabaseManager
from lib.abstract_models import MessageUpdate, Message, CallbackQuery


class Controller:

    def __init__(self):
        self.dbm = DatabaseManager()

        self.command_map = self.construct_command_map()

    def construct_command_map(self):
        init_states = self.dbm.get_children_of_state(
            self.dbm.get_root_state().id
        )
        return dict(zip(
            [state.command for state in init_states],
            [state.id for state in init_states],
        ))

    @staticmethod
    def construct_func(module_name: str = "command_funcs", func_name: str = "start", **kwargs) -> str:
        return (
                module_name
                + "."
                + func_name
                + "("
                + ",".join([f"{key}={value}" for key, value in kwargs.items()])
                + ")"
        )

    def construct_help_message(
            self,
            update: MessageUpdate,
            *args, **kwargs
    ) -> Message:
        chat_id = update.chat_id
        chat = self.dbm.get_chat(chat_id=chat_id)
        state = self.dbm.get_state(state_id=chat.state)
        commands = self.dbm.get_children_commands(state_id=state.id)

        if len(commands) == 0:
            message = Message(chat_id, "No possible commands.", {})
            message.append_inline_keyboard(text="Return to start", callback_data="/start")
            return message

        message = Message(chat_id, "List of possible commands:", {})
        for command in commands:
            message.append_inline_keyboard(
                text=command["command"], callback_data=command["command"]
            )
        return message

    def router(self, update: MessageUpdate, *args, **kwargs):
        if isinstance(update, MessageUpdate):
            return self.handle_command(update, *args, **kwargs)
        elif isinstance(update, CallbackQuery):
            return Message(update.chat_id, text="You pressed the button:)")

    def handle_command(self, update: MessageUpdate, *args, **kwargs) -> Message:
        command = update.message_text

        chat = self.dbm.get_or_create_chat(update.chat_id)
        if not chat.has_state():
            root_state = self.dbm.get_root_state()
            chat.state = root_state.id

        if command == "/help":
            return self.construct_help_message(update)

        elif command in self.command_map:
            map_state = self.dbm.get_state(self.command_map[command])

            chat.state = map_state.id

            message = Message(
                chat_id=chat.id,
                text=chat.state.text,
            )
            return message

