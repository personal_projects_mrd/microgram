import os

from lib import MicroBot, Message
from controller import Controller

TOKEN = os.getenv("token")

bot = MicroBot(token=TOKEN)
controller = Controller()


def main():
    for update in bot.check_updates():
        message = controller.router(update=update)
        if message is not None:
            bot.send_message(message=message)
        # if update.message_text.startswith("/"):
        #     user = db_manager.get_or_create_chat(db=session, chat_id=update.chat_id)
        #
        #     if user.state is None:
        #         root_state = db_manager.get_root_state(db=session)
        #         if update.message_text == root_state.command:
        #             message = Message(update.chat_id, root_state.text, {})
        #             user.state = root_state.id
        #         else:
        #             message = Message(
        #                 update.chat_id,
        #                 f"Type {root_state.command} to start!",
        #                 {}
        #             )
        #     else:
        #         message = db_manager.handle_command(db=session, update=update)
        #
        #     bot.send_message(message=message)

        # db_chat = utils.get_or_create_chat(db=session, chat_id=message.chat.id)
        # state = utils.get_state(db=session, state_id=db_chat.state)
        # children = utils.get_children_of_state(db=session, parent_id=state.id)

        # if message.text == "/help":
        #     bot.send_message()
        # for child in children:
        #     bot.send_message()
        # if state.function is not None:
        #     function_call = utils.construct_func(
        #         "command_funcs", state.function, test=True, test1=True
        #     )
        #     bot.send_message(user=message.user, content=state.reply)
        #     bot.send_message(user=message.user, content=eval(function_call))


if __name__ == "__main__":
    while 1:
        main()
