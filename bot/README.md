## `Microgram lib with an example`

###fill database with states data

```shell
python fill_states_db.py
```

###setup for containerization
```shell
cat env.local
echo token=<tgbot_token>\n > env.local
echo database_url=<database_url> > env.local
```

###build docker container
```shell
not implemented yet
```