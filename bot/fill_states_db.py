import os
import json
import logging
from datetime import datetime

from lib.models import State
from lib.database import SessionLocal


session = SessionLocal()


states = {
    "root": {
        "command": "/start",
        "text": "this is initial message. hello! type /help to get list of commands!",
        "input_type": "command",
        "data_validation": "start_validation",  # checks if there is command in children commands as an example
        "children": [
            {
                "command": "/add_task",
                "text": "enter the description of task",
                "input_type": "text",
                "data_validation": "description_validation_add_task",
            },
            {
                "command": "/add_schedule",
                "text": "enter the name of task in schedule",
                "input_type": "text",
                "data_validation": "description_validation_add_schedule"
            },
            {
                "command": "/sh_tasks",
                "text": "list of tasks:",
                "input_type": "keyboard",
                "data_validation": "blank",
            },
            {
                "command": "/sh_schedule",
                "text": "choose day of the week",
                "input_type": "keyboard",
                "data_validation": "blank",
            },
            {
                "command": "/help",
                "text": "help message",
                "input_type": "keyboard",
                "data_validation": "blank",
            }
        ],
    }
}


def add_node(state, _session, parent=None, data_validation=None):
    if state.get("function"):
        data_validation = state["function"]
    obj = State(
        command=state["command"],
        text=state["text"],
        input_type=state["input_type"],
        parent=parent,
        data_validation=data_validation,
    )
    _session.add(obj)

    logging.info(f"Added state {repr(obj)}")

    if state.get("children"):
        if len(state["children"]) > 0:
            for child in state["children"]:
                add_node(child, _session, parent=obj)


def is_possible_json(obj):
    try:
        json.dumps(obj)
        return True
    except json.JSONDecodeError:
        return False


if __name__ == "__main__":
    add_node(states["root"], session)
    session.commit()

    if is_possible_json(states):
        directory = f"{os.getcwd()}/states_dump"
        if not os.path.exists(directory):
            os.makedirs(directory)

        abs_path = f"{directory}/states_dump_{datetime.now()}.json"
        with open(abs_path, "w") as file:
            file.write(json.dumps(states))
            logging.info(f"Created dump of states at {abs_path}")
    else:
        logging.warning("Impossible to dump states!")
