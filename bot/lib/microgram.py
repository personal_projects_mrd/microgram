import os
import requests
import logging
import json
from datetime import datetime

from .abstract_models import MessageUpdate, Message, CallbackQuery


class MicroBot:

    def __init__(self, token):
        self.debug = bool(os.environ.get("debug", False))
        if self.debug:
            logging.basicConfig(filemode="bot.log", level=logging.DEBUG)
            logging.debug(f"{datetime.now()} - Initialized bot object.")
        self.token = token
        self.offset = None
        while self.offset is None:
            self.offset = self.initial_get_offset()

    def initial_get_offset(self):
        r = requests.get(
            f"https://api.telegram.org/bot{self.token}/getUpdates?timeout=300"
        )
        data = r.json()

        if "result" in data:
            if len(data["result"]) > 0:
                return data["result"][0]["update_id"] + 1

        return None

    def check_updates(self, offset: int = None, timeout: int = 120):
        self.offset = offset if offset else self.offset
        data = requests.get(
            f"https://api.telegram.org/bot{self.token}/getUpdates?offset={self.offset}&timeout={timeout}"
        ).json()

        if self.debug:
            logging.debug(f"{datetime.now()} - Got request: {json.dumps(data)}")

        if "result" in data:
            for update in data["result"]:
                update = Parser.parse_update(update)
                self.offset = update.update_id + 1
                yield update

    def send_message(self, message):
        if isinstance(message, Message):
            if not message.has_reply_markup():
                response = requests.post(
                    f"https://api.telegram.org/bot{self.token}/sendMessage",
                    data={
                        "chat_id": message.chat_id,
                        "text": message.text,
                    },
                )
            else:
                response = requests.post(
                    f"https://api.telegram.org/bot{self.token}/sendMessage",
                    data={
                        "chat_id": message.chat_id,
                        "text": message.text,
                        "reply_markup": json.dumps(message.reply_markup),
                    },
                )
            return response
        else:
            raise Exception("message attribute must be Message object")


class Parser:

    @staticmethod
    def parse_update(update):
        if update.get("callback_query"):
            cq = update["callback_query"]
            cq_update = CallbackQuery(
                update["update_id"],
                cq["message"]["message_id"],
                cq["message"]["chat"]["id"],
                cq["data"],
            )
            return cq_update
        if MessageUpdate.is_valid(update):
            abstract_update = MessageUpdate(
                update["update_id"],
                update["message"]["text"],
                update["message"]["chat"]["id"],
            )
            return abstract_update
        return None
