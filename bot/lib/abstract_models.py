from dataclasses import dataclass


@dataclass
class MessageUpdate:
    update_id: int
    message_text: str
    chat_id: int

    @staticmethod
    def is_valid(data: dict) -> bool:
        try:
            return data["message"]["text"] == data["message"]["text"]
        except KeyError:
            return False


@dataclass
class CallbackQuery:
    update_id: int
    message_id: int
    chat_id: int
    message_text: str


@dataclass
class Message:
    chat_id: int
    text: str
    reply_markup: dict = None

    def add_inline_keyboard(self):
        self.reply_markup = {"inline_keyboard": []}

    def append_inline_keyboard(self, text: str, callback_data: str):
        if not self.reply_markup:
            self.add_inline_keyboard()

        self.reply_markup["inline_keyboard"].append(
            [{"text": text, "callback_data": callback_data}]
        )

    def has_reply_markup(self):
        return (
            True
            if self.reply_markup is not {} and isinstance(self.reply_markup, dict)
            else False
        )
