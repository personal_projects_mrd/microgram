from sqlalchemy import func

from .models import Chat, State
from .microgram import Message, MessageUpdate
from .database import SessionLocal


class DatabaseManager:

    def __init__(self):
        self.db = SessionLocal()

    def get_or_create_chat(self, chat_id: int):
        chat = self.get_chat(chat_id=chat_id)

        if chat is None:
            return self.create_chat(chat_id=chat_id)
        return chat

    def get_chat(self, chat_id: int):
        return self.db.query(Chat).filter(Chat.id == chat_id).first()

    def create_chat(self, chat_id: int):
        chat = Chat(id=chat_id)

        self.db.add(chat)
        self.db.commit()
        self.db.refresh(chat)
        return chat

    def get_all_states(self):
        return self.db.query(State).all()

    def get_root_state(self):
        return self.get_all_states()[0]

    def get_state(self, state_id: int):
        return self.db.query(State).filter(State.id == state_id).first()

    def get_children_of_state(self, parent_id: int):
        state = self.get_state(state_id=parent_id)
        return state.children

    def get_siblings_of_state(self, state_id: int):
        state = self.db.query(State).filter_by(id=state_id).one()
        return (
            self.db.query(State)
            .filter(
                State.path.descendant_of(state.path[:-1]),
                func.nlevel(State.path) == len(state.path),
                State.id != state.id,
            )
            .all()
        )

    def get_children_commands(self, state_id: int) -> list:
        children = self.get_children_of_state(parent_id=state_id)

        children_commands = list()
        for child in children:
            children_commands.append(
                {"help_message": child.text, "command": child.command}
            )

        return children_commands

def construct_dev_message(update: MessageUpdate, *args, **kwargs) -> Message:
    text = "Developer: Mingazov Rashid\n" \
           "Mail: rdmingazov@gmail.com\n" \
           "Repository: https://gitlab.com/personal_projects_mrd/microgram"

    return Message(update.chat_id, text, {})

