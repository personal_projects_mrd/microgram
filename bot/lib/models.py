from sqlalchemy import Column, ForeignKey, Integer, String, Sequence, func, Index
from sqlalchemy.orm import relationship, remote, foreign
from sqlalchemy_utils import LtreeType, Ltree

from .database import Base, engine


id_seq = Sequence("states_id_seq")


class State(Base):
    __tablename__ = "states"

    id = Column(Integer, id_seq, primary_key=True)
    command = Column(String, nullable=False)
    text = Column(String, nullable=False)
    input_type = Column(String, nullable=False)
    data_validation = Column(String, nullable=True)
    path = Column(LtreeType, nullable=True)
    parent = relationship(
        "State",
        primaryjoin=(remote(path) == foreign(func.subpath(path, 0, -1))),
        backref="children",
        viewonly=True,
    )
    chat = relationship("Chat")

    __table_args__ = (Index("ix_nodes_path", path, postgresql_using="gist"),)

    def __init__(self, command, text, input_type, data_validation=None, parent=None):
        _id = engine.execute(id_seq)
        self.id = _id
        self.command = command
        self.text = text
        self.input_type = input_type
        self.data_validation = data_validation
        ltree_id = Ltree(str(_id))
        self.path = ltree_id if parent is None else parent.path + ltree_id

    def __str__(self):
        return self.command

    def __repr__(self):
        return f"{self.id} - {self.command}"


class Chat(Base):
    __tablename__ = "chats"

    id = Column(Integer, primary_key=True, index=True)

    state = Column(Integer, ForeignKey("states.id"), nullable=True)

    def has_state(self):
        if self.state is None:
            return False
        else:
            return True

    def __str__(self):
        return f"{self.id}"

    def __repr__(self):
        return f"{self.id}"


Base.metadata.create_all(engine)
