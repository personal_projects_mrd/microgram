import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()

engine = create_engine(os.getenv("database_url"))
SessionLocal = sessionmaker(bind=engine)
